﻿using System;
using System.Collections.Generic;
using LEGOModelImporter;
using UnityEngine;

namespace _LEGO_Extended.Scripts.Utilities
{
    [Serializable]
    public class DecorationSurfaceSide
    {
        public string name;
        public bool enabled;
        public Material material;
        public MeshRenderer renderer;
    }
    
    /**
     * Start by checking and identify each decoration surface
     * Allow to enable some surfaces
     * Allow to change the material
     */
    public class DecorationSurfaceEditor : MonoBehaviour
    {
        [SerializeField]
        protected Brick m_Brick;
        
        [SerializeField]
        protected List<DecorationSurfaceSide> m_DecorationSurfaceSides = new List<DecorationSurfaceSide>();

        protected void Reset()
        {
            m_Brick = GetComponent<Brick>();
            if (!m_Brick)
            {
                // TODO: Manage exception du to brick missing
                return;
            }
            
            foreach(var part in m_Brick.parts)
            {
                var partRenderers = part.GetComponentsInChildren<MeshRenderer>(true);
                foreach (var partRenderer in partRenderers)
                {
                    if (partRenderer.transform.parent && partRenderer.transform.parent.name == "DecorationSurfaces")
                    {
                        var side = new DecorationSurfaceSide();
                        side.name = partRenderer.name;
                        side.enabled = partRenderer.gameObject.activeInHierarchy;
                        side.material = partRenderer.sharedMaterial;
                        side.renderer = partRenderer;
                        
                        m_DecorationSurfaceSides.Add(side);
                    }
                }
            }
        }

        private void OnValidate()
        {
            foreach (DecorationSurfaceSide side in m_DecorationSurfaceSides)
            {
                side.renderer.sharedMaterial = side.material;
                side.renderer.gameObject.SetActive(side.enabled);

                Transform parentDecorator = side.renderer.transform.parent;
                bool decorationActive = false;
                if (side.enabled)
                    decorationActive = true;
                else
                    foreach (Transform siblings in side.renderer.transform.parent)
                        decorationActive = decorationActive || siblings.gameObject.activeSelf;
                parentDecorator.gameObject.SetActive(decorationActive);
            }
        }
    }
}