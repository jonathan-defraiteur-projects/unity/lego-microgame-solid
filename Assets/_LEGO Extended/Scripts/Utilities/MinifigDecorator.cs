﻿using System;
using UnityEngine;

namespace _LEGO_Extended.Scripts.Utilities
{
    public class MinifigDecorator : MonoBehaviour
    {
        private void Reset()
        {
            RecursiveUnhide(transform);
        }

        protected void RecursiveUnhide(Transform transform)
        {
            transform.gameObject.hideFlags = HideFlags.None;
            foreach (Transform child in transform)
            {
                RecursiveUnhide(child);
            }
        }
    }
}