﻿using JonathanDefraiteur.LEGO_Extended.Behaviours.Actions;
using Unity.LEGO.EditorExt;
using UnityEditor;

namespace JonathanDefraiteur.LEGO_Extended.Editor
{
    [CustomEditor(typeof(UnityEventAction), true)]
    public class UnityEventActionEditor : ActionEditor
    {

        SerializedProperty m_EventProp;
        
        protected override void OnEnable()
        {
            base.OnEnable();

            m_EventProp = serializedObject.FindProperty("m_Event");

        }

        protected override void CreateGUI()
        {
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying);

            EditorGUILayout.PropertyField(m_EventProp);

            EditorGUI.EndDisabledGroup();
        }
    }
}