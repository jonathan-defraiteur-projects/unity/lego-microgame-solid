using JonathanDefraiteur.LEGO_Extended.Behaviours.Actions;
using Unity.LEGO.EditorExt;
using UnityEditor;

namespace JonathanDefraiteur.LEGO_Extended.Editor
{
    [CustomEditor(typeof(SceneAction), true)]
    public class SceneActionEditor : ActionEditor
    {

        SerializedProperty m_SceneProp;
        
        protected override void OnEnable()
        {
            base.OnEnable();

            m_SceneProp = serializedObject.FindProperty("m_Scene");

        }

        protected override void CreateGUI()
        {
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlaying);

            EditorGUILayout.PropertyField(m_SceneProp);

            EditorGUI.EndDisabledGroup();
        }
    }
}