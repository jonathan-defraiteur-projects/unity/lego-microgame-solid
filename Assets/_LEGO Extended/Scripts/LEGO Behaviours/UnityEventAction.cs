﻿using UnityEngine;
using UnityEngine.Events;

namespace JonathanDefraiteur.LEGO_Extended.Behaviours.Actions
{
    public class UnityEventAction : Unity.LEGO.Behaviours.Actions.Action
    {
        [SerializeField]
        private UnityEvent m_Event;

        protected void Update()
        {
            if (!m_Active)
                return;
            
            m_Event.Invoke();
            m_Active = false;
        }

        protected override void Start()
        {
            base.Start();

            RecursiveUnhide(transform);
        }

        protected void RecursiveUnhide(Transform transform)
        {
            transform.gameObject.hideFlags = HideFlags.None;
            foreach (Transform child in transform)
            {
                RecursiveUnhide(child);
            }
        }
    }
}